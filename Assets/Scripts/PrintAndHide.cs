using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrintAndHide : MonoBehaviour
{
    public GameObject obj;
    private int i;
    public Renderer rend;
    private int random;
    // Start is called before the first frame update
    void Start()
    {
        i = 3;
        random = Random.Range(200, 251);
    }

    // Update is called once per frame
    void Update()
    {
        if(obj.tag=="Red" && i == 100)
        {
            obj.SetActive(false);
        }
        if (obj.tag == "Blue" && i == random)
        {
            obj.GetComponent<Renderer>().enabled = !obj.GetComponent<Renderer>().enabled;
        }
        i += 1;
        Debug.Log(obj.name +":"+i);
    }
}
